import os
from tornado.escape import (
    json_decode,
    json_encode,
    to_unicode,
)
from tornado.ioloop import IOLoop
from tornado.process import Subprocess
from tornado.web import (
    Application,
    RequestHandler,
)


COMMAND = os.environ.get('CMDASS_COMMAND', '/bin/bash')
ARGS = os.environ.get('CMDASS_ARGS', '-c')


class MainHandler(RequestHandler):

    async def post(self):
        data = json_decode(self.request.body)
        options = data.get('options', {})
        args = data.get('args', [])
        result = await self.call_subprocess(args, options)
        self.write(json_encode(result))

    def flat_args(self, args, options):
        flatten = [COMMAND, ARGS]
        flatten.extend(args)
        for option, value in options:
            flatten.append(option)
            if value is not None:
                flatten.append(value)
        return flatten

    async def call_subprocess(self, args, options):
        flatten = self.flat_args(args, options)
        process = Subprocess(
            flatten,
            stdout=Subprocess.STREAM,
            stderr=Subprocess.STREAM,
        )
        stdout = await process.stdout.read_until_close()
        stderr = await process.stderr.read_until_close()
        retcode = await process.wait_for_exit()
        result = dict(
            stdout=to_unicode(stdout),
            stderr=to_unicode(stderr),
            retcode=retcode,
        )
        return result


def make_app():
    return Application([
        (r"/call", MainHandler),
    ])


if __name__ == "__main__":
    app = make_app()
    app.listen(8888)
    IOLoop.current().start()
